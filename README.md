# prakpy
Výukový nastroj pro použití programovacího jazyka python při zpracování dat z měření.  
Využívá interaktivní prostředí pro python [jupyter notebook](https://jupyter.readthedocs.io/en/latest/install/notebook-classic.html).  
Bývá součástí distribucí python jako například [annaconda](https://www.anaconda.com/products/individual).

## Obsah

### Tutoriály:
 - základy práce s proměnnými v pythonu [prace_s_daty](prace_s_daty.ipynb)
    - indexování
    - načítání z txt
    - práce se slovníkem
 - zpracovaní přímého měření [zpracovani](zpracovani.ipynb)
    - průměr
    - směrodatná odchylka
    - chyba průměru
    - výpis do tex formátu
 - základní vykreslení grafů [grafy](grafy.ipynb)
    - formátování
    - chybové úsečky (errorbary)
    - ukládání
 - prokládání dat [fity](fity.ipynb)
 - odvození vzorce pro chybu nepřímého měření pomocí symbolického počítání [symbolics](symbolics.ipynb)
 - vykreslení dvourozměrných dat [2D_grafy](2D_grafy.ipynb)
 - numerická integrace dat [numerika](numerika.ipynb)
 - spouštění python skriptů [spousteni](spousteni.ipynb)

### Teorie:
 - Chyby a rozdělovací funkce [chyby](chyby.ipynb)

### Příklady:
 - [Extrapolace](Extrapolace.ipynb)
 - [Hobit](Hobit.ipynb)
 - možné způsoby [citace](CITE.txt)

## dokumentace k použitým knihovnám
 - [numpy](https://docs.scipy.org/doc/numpy/reference/index.html)
 - [matplotlib](https://matplotlib.org/contents.html)
 - [scipy](https://docs.scipy.org/doc/scipy/reference/)

*********
verze 22.09
