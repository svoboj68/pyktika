import numpy as np


def sem(pole):
    """Standard Error of Mean"""
    return pole.std() / np.sqrt(pole.size-1)


def sem_annotated(pole: np.ndarray) -> float:
    """
    Spocita chybu prumeru

    Funguje uplne stejne jako sem, akorat jsou zaneseny typy parametru a vystupu

    Parameters
    ----------
    pole : np.ndarray
        hodnoty 

    Returns
    -------
    float
        chyba prumeru
    """
    return pole.std() / np.sqrt(pole.size-1)


def foo():
    print('bar')
    return
